<?php

namespace CodeClans\Notification\Controllers;

use App\Http\Controllers\Controller;
use CodeClans\Notification\Models\UserFcmToken;
use CodeClans\Notification\Models\UserNotification;
use Yajra\DataTables\Facades\DataTables;

class NotificationController extends Controller{

    public static function listNotificationView()
    {
        return view('front.template.user_notification.index');
    }

    public static function listNotificationsWithView()
    {
        if(!auth()->user()){
            return response()->json(['message' => 'unauthorized!'], 401);
        }

        $records = UserNotification::where(['user_id' => auth()->user()->id, 'channel' => 'WEB'])
        ->select('*');

        return DataTables::of($records)
            ->addColumn('created_at', function($query) {
                return date("d/m/Y", strtotime($query->created_at));
            })
            ->addIndexColumn()
            ->make(true);
    }

    //admin data table listing
    public static function adminNotificationView()
    {
        return view('admin.notification.index');
    }

    public static function adminListNotificationsWithView()
    {
        if(!auth()->user()){
            return response()->json(['message' => 'unauthorized!'], 401);
        }

        $records = UserNotification::where(['user_id' => auth()->user()->id])
        ->select('*');
        return DataTables::of($records)
            ->addIndexColumn()
            ->make(true);
    }

    public function registerFcmToken()
    {
        $fcmToken = UserFcmToken::where('user_id', auth()->user()->id)->where('fcm_token', request()->fcm_token)->first();
        if($fcmToken){
            $fcmToken->update(['fcm_token' => request()->fcm_token]);
        }else{
            $fcmToken = UserFcmToken::create(['user_id' => auth()->user()->id, 'fcm_token' => request()->fcm_token, 'device_type' => request()->device_type]);
        }
        if($fcmToken){
            return response()->json(['data' => ['message' => 'fcm token update successfully']], 200);
        }
        return response()->json(['data'=> ['message' => 'fcm token update failed']], 400);
    }

    public static function listNotifications($limit = 4)
    {
        if(!auth()->user()){
            return response()->json(['message' => 'unauthorized!'], 401);
        }
        $channel = request()->channel ?? null;
        $items = UserNotification::where(['user_id' => auth()->user()->id, 'is_read' => false])
        ->where(function($query) use($channel){
            if($channel !== null){
                $query->where('channel', $channel);
            }
        })
        ->join('users','user_id', 'users.id')
        ->select('user_notifications.*', 'users.name as username');
        $notifications = $items->orderBy('user_notifications.id','desc')->limit($limit)->get();
        if(count($notifications) > 0){
            return response()->json(['data'=> $notifications->toArray(), 'cntUnread' => $items->count(), 'message' => 'Notification lists successfully', 'count' => $items->count()], 200);
        }
        
        return response()->json(['data'=> [], 'cntUnread' => 0, 'message' => 'No data available', 'count' => 0], 200);

    }

    public static function listNotificationByChannel($channel)
    {
        if(!$channel){
            return response()->json(['message' => 'Channel type is required!'], 404);
        }
        $notifications = UserNotification::where('channel', $channel)->where('is_read', false)->get();
        if($notifications){
            return response()->json(['data'=> $notifications, 'message' => 'Data notification lists successfully'], 200);
        }
        return response()->json(['data'=> [], 'message' => 'No data available'], 400);
    }

    public static function getNotificationById($id)
    {
        if(!$id){
            return response()->json(['message' => 'Notification id is required!'], 404);
        }
        $notifications = UserNotification::where('id', $id)->where('is_read', false)->get();
        if($notifications){
            return response()->json(['data'=> $notifications, 'message' => 'Notification found.'], 200);
        }
        return response()->json(['data'=> [], 'message' => 'No data available'], 400);
    }

    public static function markNotificationAsRead($id)
    {
        if(!$id){
            return response()->json(['message' => 'Notification id is required!'], 404);
        }
        $notification = UserNotification::where('id', $id)->first();
        if($notification){
            $updated = $notification->update(['is_read' => true]);
            if($updated){
                return response()->json(['data' =>$notification, 'message' => 'Notification has been mark as read.'], 200);
            }
        }
        return response()->json(['data'=> [], 'message' => 'No data available'], 400);
    }
}
