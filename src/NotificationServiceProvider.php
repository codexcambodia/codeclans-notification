<?php
namespace CodeClans\Notification;
use Illuminate\Support\ServiceProvider;

class NotificationServiceProvider extends ServiceProvider
{
    /**
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/database/migrations' => base_path('database/migrations'),
                __DIR__ . '/public/firebase-messaging-sw.js' => base_path('public/firebase-messaging-sw.js')
            ], 'codeclans-notification');

        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        
    }

    

    /**
     * @return array
     */
    public function provides()
    {
        return [];
    }
}