<?php
namespace CodeClans\Notification\Helpers;

use App\Domains\Notification\Notification as NotificationModel;
use App\Domains\UserNotification\UserNotification;
use CodeClans\Notification\Controllers\NotificationController;
use CodeClans\Notification\Models\UserFcmToken;
use Illuminate\Support\Facades\Route;

class AdminNotification
{
    
  public static function sendMultipleUser($userIds, $title, $body, $image, $channel, $type, $identifier, $notificationId = null)
  {
      $from = env('FIREBASE_KEY', 'AAAADzXSIJg:APA91bEhiTI3W043USmSwkxsqynWqNX0aZibrlB1XZJO8hsWtvagTEMRszeKxkFyCEf1nwv-RAKm8FR1GSaaPWUkbYiwemfCKP_n6uRnzUFwHT5lONWJn6KiIklJ9TEuE6oF8BG61KuP');
      $status = false;
      $fcmTokens = UserFcmToken::whereIn('user_id', $userIds)->pluck('fcm_token')->toArray();
      $arrUserIds = UserFcmToken::whereIn('user_id', $userIds)->pluck('user_id')->toArray();

      if($fcmTokens){
        //validate 
        if(!$title){
          return ['status' => $status, 'message' => 'Title is required!'];
        }
        if(!$body){
          return ['status' => $status, 'message' => 'Body description is required!'];
        }
        if(!$channel){
          return ['status' => $status, 'message' => 'Channel is required!'];
        }
        if(!$type){
          return ['status' => $status, 'message' => 'Type is required!'];
        }
        if(!$identifier){
          return ['status' => $status, 'message' => 'Identifier is required!'];
        }
        
        $msg = array(
          'body'  => $body,
          'title' => $title,
          'icon'  => $image ?? "https://image.flaticon.com/icons/png/512/270/270014.png",/*Default Icon*/
          'sound' => 'mySound'/*Default sound*/
        );
        $fields = array(
            'registration_ids' => $fcmTokens,
            'notification' => $msg
        );
        $headers = array(
            'Authorization: key=' . $from,
            'Content-Type: application/json'
        );
        //#Send Reponse To FireBase Server 
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        if(json_decode($result)->success){
          $status = true;
        }
        curl_close( $ch );
        //insert to user notification and notification table
        $notification = [
          'channel' => $channel,
          'type' => $type ? $type : 1,
          'identifier' => $identifier ? $identifier : 0,
          'title' => $title,
          'body' => $body,
          'image' => $image ? $image : "https://image.flaticon.com/icons/png/512/270/270014.png",
        ];

        $notic = NotificationModel::create($notification);
        foreach ($arrUserIds as $userId) {
          UserNotification::create($notification + ['notification_id' => $notic->id, 'is_read' => false , 'user_id' => $userId]);
        }
        return ['status' => $status, 'message' => 'Send notification successfully!'];
      }
    
    return ['status' => $status, 'message' => 'Send notification failed!'];
  }

  public static function send($userId, $title, $body, $image, $channel, $type, $identifier, $notificationId = null)
  {
      $from = env('FIREBASE_KEY', 'AAAADzXSIJg:APA91bEhiTI3W043USmSwkxsqynWqNX0aZibrlB1XZJO8hsWtvagTEMRszeKxkFyCEf1nwv-RAKm8FR1GSaaPWUkbYiwemfCKP_n6uRnzUFwHT5lONWJn6KiIklJ9TEuE6oF8BG61KuP');
      $status = false;
      $fcmToken = UserFcmToken::where('user_id', $userId)->first();
      if($fcmToken && $fcmToken->fcm_token){
        //validate 
        if(!$title){
          return ['status' => $status, 'message' => 'Title is required!'];
        }
        if(!$body){
          return ['status' => $status, 'message' => 'Body description is required!'];
        }
        if(!$channel){
          return ['status' => $status, 'message' => 'Channel is required!'];
        }
        if(!$type){
          return ['status' => $status, 'message' => 'Type is required!'];
        }
        if(!$identifier){
          return ['status' => $status, 'message' => 'Identifier is required!'];
        }
        
        $msg = array(
          'body'  => $body,
          'title' => $title,
          'icon'  => $image ?? "https://image.flaticon.com/icons/png/512/270/270014.png",/*Default Icon*/
          'sound' => 'mySound'/*Default sound*/
        );
        $fields = array(
            'to'        => $fcmToken->fcm_token,
            'notification'  => $msg
        );
        $headers = array(
            'Authorization: key=' . $from,
            'Content-Type: application/json'
        );
        //#Send Reponse To FireBase Server 
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        if(json_decode($result)->success){
          $status = true;
          //insert to user notification and notification table
          $notification = [
            'channel' => $channel,
            'type' => $type ? $type : 1,
            'identifier' => $identifier ? $identifier : 0, //owner user id
            'title' => $title,
            'body' => $body,
            'image' => $image ? $image : "https://image.flaticon.com/icons/png/512/270/270014.png",
          ];
          
          $notic = NotificationModel::create($notification);
          if($notic)
            UserNotification::create($notification + ['notification_id' => $notic->id, 'is_read' => false , 'user_id' => $userId]); // send to user id
        }
        curl_close( $ch );
        return ['status' => $status, 'message' => 'Send notification successfully!'];
      }
    
    return ['status' => $status, 'message' => 'Send notification failed!'];
  }

  public static function registerRouteApiAdmin(String $routePrefix, String $urlPrefix){
    //route for register fcm token
    Route::get($urlPrefix.'register-fcm-token',[ NotificationController::class, 'registerFcmToken' ])->name($routePrefix.'.'.'register-fcm-token');
    //route for list all notification
    Route::get($urlPrefix.'user-notification',[ NotificationController::class, 'listNotifications' ])->name($routePrefix.'.'.'list-notification');
    //============================= Render In View Using Data Table ================================
    //route for list all notification
    Route::get($urlPrefix.'user-list',[ NotificationController::class, 'adminNotificationView' ])->name($routePrefix.'.'.'view-notification');
    //This route is call in script js request by ajax data table
    Route::get($urlPrefix.'user-indexData',[ NotificationController::class, 'adminListNotificationsWithView' ])->name($routePrefix.'.'.'list-all-notification');
    //============================= End Render Data Table ================================
    //route for list notification by channel
    Route::get($urlPrefix.'user-notification/channel/{channel}',[ NotificationController::class, 'listNotificationByChannel' ])->name($routePrefix.'.'.'list-channel-notification');
    //route for notification detail by id
    Route::get($urlPrefix.'user-notification/{id}',[ NotificationController::class, 'getNotificationById' ])->name($routePrefix.'.'.'get-detail-notification');
    //route for mark as read
    Route::get($urlPrefix.'user-notification/{id}/read',[ NotificationController::class, 'markNotificationAsRead' ])->name($routePrefix.'.'.'mark-read-notification');
  }

  public static function registerFirebaseMessagingScript(String $firebaseConfing, String $messagingPublicKey, String $routeRegisterFcm){
      return '<script src="https://www.gstatic.com/firebasejs/8.6.1/firebase-app.js"></script>
      <script src="https://www.gstatic.com/firebasejs/8.6.1/firebase-messaging.js"></script>
      <script>
        var firebaseConfig = '.$firebaseConfing.';
        // Initialize Firebase
        firebase.initializeApp(firebaseConfig);
      
        const messaging = firebase.messaging();
        
      // Get registration token. Initially this makes a network call, once retrieved
      // subsequent calls to getToken will return from cache.
      messaging.getToken({ vapidKey: \''.$messagingPublicKey.'\' }).then((currentToken) => {
        if (currentToken) {
            var registerFcmUrl = \''. route($routeRegisterFcm) .'\';
            // console.log(\'fcmToken: \'+currentToken);
            // console.log(\'registerFcmUrl: \'+registerFcmUrl);
            $.ajax({
              type: "GET",
              url: registerFcmUrl,
              data: {"fcm_token":currentToken, "device_type": "WEB"}
            }).done(function (msg) {
              // console.log(msg.data.message);
            });
        } else {
          
        }
      }).catch((err) => {
        console.log(\'An error occurred while retrieving token. \', err);
        
      });
      messaging.onMessage((payload) => {
        
        const notificationOption = {
            body: payload.notification.body
        }
        var notification = new Notification(payload.notification.title,notificationOption);
      
      });
      </script>';
  }
  public static function notificationLists(String $routeListNotification){
    return '
    <script>
      var urlListNotification = \''. route($routeListNotification) .'\';
      var now = moment();

      $.ajax({
        type: "GET",
        url: urlListNotification,
        data: {"channel": "WEB"}
      }).done(function (obj) {
        var element = "";
        var cntUnreadNotic = obj.cntUnread;

        $("#lists-notification").empty();
        $("#cnt-unread-notic").empty();
        $("#cnt-badge-notic").empty();
        $("#old-lists-notification").empty();

        var ele2 =\'<li class="text-divider small pb-2 pl-3 pt-3">\';
            ele2 +=\'    <span>Old notifications</span>\';
            ele2 +=\'</li>\';

        $.each( obj.data, function( key, value ) {
          var date = moment(value.created_at, "YYYYMMDD").fromNow();
          var urlMarkRead = \''.url('notification/user-notification/\'+value.id+\'/read').'\';
          var urImage = \''.url('assets/images/avatars/avatar2.png').'\';

          if(value.is_read == false || value.is_read == "false"){
            element +=\'<a href="#" class="unread-row dropdown-item notify-item">\';
            element +=\'    <div class="notify-icon bg-faded">\';
            element +=\'        <img src=\'+urImage+\' alt="img" class="rounded-circle img-fluid">\';
            element +=\'    </div>\';
            element +=\'    <p data-url=\'+urlMarkRead+\' onclick="ViewMarkRead(this);" class="notify-details">\';
            element +=\'        <b>\'+value.username+\'</b>\';
            element +=\'        <span>\'+value.title+\'</span>\';
            element +=\'        <small class="text-muted">\'+date+\'</small>\';
            element +=\'    </p>\';
            element +=\'</a>\';

          }
        });
        //push to element unread
        $("#lists-notification").html(element);
        //push to count unread notification
        if(cntUnreadNotic > 0){
          $("#cnt-badge-notic").html(cntUnreadNotic);
        }else{
          $("#cnt-badge-notic").html(\'\');
        }
        
      });

      //function mark as read
      function markRead(el) {
        var obj = $(el);
        var url = obj.attr(\'data-url\');
        $.ajax({
          type: "GET",
          url: url,
          data: {}
        }).done(function (msg) {
          // console.log(msg.message);
          location.reload();
        });
      };

      //function view mark as read
      function ViewMarkRead(el) {
        var obj = $(el);
        var url = obj.attr(\'data-url\');
        //append new element when read element not reach 4 row
        var cntReadRow = $("#old-lists-notification").length;
        if(cntReadRow >= 0 && cntReadRow < 4){
          $("#old-lists-notification").append(obj.closest("a.unread-row").html());
        }
        //remove li element
        obj.closest("a.unread-row").remove();

        $.ajax({
          type: "GET",
          url: url,
          data: {}
        }).done(function (obj) {
          var data =obj.data;
          var modal = $("#viewNotificationModal");
            modal.find(".modal-header").addClass(\'bg-primary\');
            modal.find(".event-title").html("<span class=\'text-uppercase\'>"+data.title+"</span>");
            modal.find(".event-body").html("<b>Channel</b>"+"<p>"+data.channel+"</p>"+"<b>Notification Date</b>"+"<p>"+os_date_format(data.created_at)+"</p>"+"<b>Description</b>"+"<p>"+data.body+"</p>");
            modal.modal();
        });
      };

    </script>
    ';
  }
    
}