importScripts('https://www.gstatic.com/firebasejs/8.6.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.6.1/firebase-messaging.js');

// To be replace with firebaseConfig
var firebaseConfig = {
  apiKey: "AIzaSyB3eH0BrdZHNpc9jlyFQ_HozYIa0PP4jGg",
  authDomain: "laravel-notic.firebaseapp.com",
  databaseURL: "https://laravel-notic-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "laravel-notic",
  storageBucket: "laravel-notic.appspot.com",
  messagingSenderId: "65327472792",
  appId: "1:65327472792:web:317c2f026460d0dd2ff0ed",
  measurementId: "G-0Z83C3SG6P"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging();

messaging.onBackgroundMessage(function(payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
  const notificationTitle = 'Background Message Title';
  const notificationOptions = {
    body: 'Background Message body.'
  };

  self.registration.showNotification(notificationTitle,
    notificationOptions);
});
