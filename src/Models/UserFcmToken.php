<?php

/**
 * Created by Reliese Model.
 */

namespace CodeClans\Notification\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserFcmToken
 * 
 * @property int $id
 * @property int $user_id
 * @property string $fcm_token
 * @property string $device_type
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Domains\Base
 */
class UserFcmToken extends Model
{
	protected $table = 'user_fcm_tokens';

	protected $casts = [
		'user_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'fcm_token',
		'device_type'
	];
}
