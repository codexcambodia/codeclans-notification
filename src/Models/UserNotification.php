<?php

/**
 * Created by Reliese Model.
 */

namespace CodeClans\Notification\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserNotification
 * 
 * @property int $id
 * @property string $channel
 * @property string $type
 * @property string $identifier
 * @property string $title
 * @property string $body
 * @property string $image
 * @property int $notification_id
 * @property bool $is_read
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Domains\Base
 */
class UserNotification extends Model
{
	protected $table = 'user_notifications';

	protected $casts = [
		'notification_id' => 'int',
		'is_read' => 'bool'
	];

	protected $fillable = [
		'channel',
		'type',
		'identifier',
		'title',
		'body',
		'image',
		'notification_id',
		'is_read'
	];
}
