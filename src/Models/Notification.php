<?php

/**
 * Created by Reliese Model.
 */

namespace CodeClans\Notification\Models;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Notification
 * 
 * @property int $id
 * @property string $channel
 * @property string $type
 * @property string $identifier
 * @property string $title
 * @property string $body
 * @property string $image
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Domains\Base
 */
class Notification extends Model
{
	protected $table = 'notifications';

	protected $fillable = [
		'channel',
		'type',
		'identifier',
		'title',
		'body',
		'image'
	];
}
