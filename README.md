# codeclans-notification
Laravel Package to implement notification easily.

## Version
Ther is no versioning as this one for internal use only.
Currently only dev version.

## Installation

1. You need to have access to gitlab repository https://gitlab.com/codexcambodia/codeclans-notification
2. Add the repositories in composer.json
```json
    "repositories": [
        ...
        {
            "type": "vcs",
            "url": "git@gitlab.com:codexcambodia/codeclans-notification.git"
        }
        ....
    ],
```
3. Run composer to install package
```shell
    composer require codeclans/notification
```
4. Publish
```shell
    php artisan vendor:publish --tag=codeclans-notification
```
5. Register Api in the route ( web.php, api.php, or admin.php) with helper CodeClans\Notification\Helpers\Notification. Method registerRouteApi require two parameters:
- String $routePrefix
- String $urlPrefix
Example api.php:
```php
...
use CodeClans\Notification\Helpers\Notification;
...
Notification::registerRouteApi('notification','/notification/');
```
6. There will be new apis for notification

7. To enable notification for web, add the following code to blade layout and modify file public/firebase-messaging-sw.js with the correct firebase config, public key, and register-fcm-token route
```php
{!!(CodeClans\Notification\Helpers\Notification::registerFirebaseMessagingScript(
		'{
		apiKey: "AIzaSyB3eH0BrdZHNpc9jlyFQ_HozYIa0PP4jGg",
		authDomain: "laravel-notic.firebaseapp.com",
		databaseURL: "https://laravel-notic-default-rtdb.asia-southeast1.firebasedatabase.app",
		projectId: "laravel-notic",
		storageBucket: "laravel-notic.appspot.com",
		messagingSenderId: "65327472792",
		appId: "1:65327472792:web:317c2f026460d0dd2ff0ed",
		measurementId: "G-0Z83C3SG6P"
	   	}',
		'BL9DSQjrrqNUK9zDJsKtHnYPJndfIylv9fMwgrLu6QfJi_Z4rYEJJlTNk0lNNGsmrAZSuvvTC8_4jCLjrQvUKdI',
		'admin.notification.register-fcm-token'
	)) !!}
```